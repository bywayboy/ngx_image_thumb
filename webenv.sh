#!/bin/bash

export USER_GROUP=www
export USER_NAME=nginx


export TENGINE_VERSION=2.3.0
export JEMALLOC_VESION=5.2.0
export OPENSSL_VERSION=1.1.1b
export LUA_NGINX_VESION=0.10.14
export LUA_WEBSOCKET_VERSION=0.07
export LUAJIT_VERSION=2.1-20190329
export LUA_RESTY_MYSQL_VERSION=0.21


groupadd $USER_GROUP
useradd -G $USER_GROUP -M -s /usr/sbin/nologin $USER_NAME
useradd -G $USER_GROUP -M -s /usr/sbin/nologin php


apt install zlib1g-dev libgd-dev libcurl4-openssl-dev libpcre3-dev gcc cmake

# 下载tengine 依赖项.
if [ ! -d tengine-$TENGINE_VERSION ]; then
	wget -O tengine-$TENGINE_VERSION.tar.gz http://tengine.taobao.org/download/tengine-2.3.0.tar.gz
	tar -xvf tengine-$TENGINE_VERSION.tar.gz
fi

if [ ! -d jemalloc-$JEMALLOC_VESION ]; then
	wget -O jemalloc-$JEMALLOC_VESION.tar.bz2 https://github.com/jemalloc/jemalloc/releases/download/$JEMALLOC_VESION/jemalloc-$JEMALLOC_VESION.tar.bz2
	tar -xvjf jemalloc-$JEMALLOC_VESION.tar.bz2
fi

if [ ! -d openssl-$OPENSSL_VERSION ]; then
	wget -O openssl-$OPENSSL_VERSION.tar.gz https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz
	tar -xvf openssl-$OPENSSL_VERSION.tar.gz
fi

if [ ! -d lua-nginx-module-$LUA_NGINX_VESION ]; then
	wget -O lua-nginx-module-$LUA_NGINX_VESION.tar.gz https://github.com/openresty/lua-nginx-module/archive/v$LUA_NGINX_VESION.tar.gz
	tar -xvf lua-nginx-module-$LUA_NGINX_VESION.tar.gz
fi

if [ ! -d lua-nginx-module-$LUA_NGINX_VESION ]; then
	wget -O lua-nginx-module-$LUA_NGINX_VESION.tar.gz https://github.com/openresty/lua-nginx-module/archive/v$LUA_NGINX_VESION.tar.gz
	tar -xvf lua-nginx-module-$LUA_NGINX_VESION.tar.gz
fi

if [ ! -d lua-resty-websocket-$LUA_WEBSOCKET_VERSION ]; then
	wget -O lua-resty-websocket-$LUA_WEBSOCKET_VERSION.tar.gz https://github.com/openresty/lua-resty-websocket/archive/v$LUA_WEBSOCKET_VERSION.tar.gz
	tar -xvf lua-resty-websocket-$LUA_WEBSOCKET_VERSION.tar.gz
fi

if [ ! -d lua-resty-mysql-$LUA_RESTY_MYSQL_VERSION ]; then
	wget -O lua-resty-mysql-$LUA_RESTY_MYSQL_VERSION.tar.gz https://github.com/openresty/lua-resty-mysql/archive/v$LUA_RESTY_MYSQL_VERSION.tar.gz
	tar -xvf lua-resty-mysql-$LUA_RESTY_MYSQL_VERSION.tar.gz
fi

# 编译 LuaJIT
if [ ! -d luajit2-$LUAJIT_VERSION ]; then
	wget -O luajit2-$LUAJIT_VERSION.tar.gz https://github.com/openresty/luajit2/archive/v$LUAJIT_VERSION.tar.gz
	tar -xvf luajit2-$LUAJIT_VERSION.tar.gz
fi

pushd luajit2-$LUAJIT_VERSION
make && make install
popd



# 编译web服务器
pushd tengine-$TENGINE_VERSION
export LUAJIT_INC=/usr/local/include/luajit-2.1
./configure --user=$USER_NAME --group=$USER_GROUP --with-jemalloc=../jemalloc-$JEMALLOC_VESION --with-openssl=../openssl-$OPENSSL_VERSION \
--add-module=../lua-nginx-module-$LUA_NGINX_VESION --with-luajit-lib=/usr/local/lib --with-luajit-inc=/usr/local/include/luajit-2.1 \
--add-module=modules/ngx_http_concat_module \
--add-module=../ngx_image_thumb \
--with-http_realip_module \
--with-http_v2_module \
--with-threads \


make -j3 && make install

popd

cat  <<EOF 	> /lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=syslog.target network.target remote-fs.target nss-lookup.target
 
[Service]
Type=forking
PIDFile=/usr/local/nginx/logs/nginx.pid
ExecStartPre=/usr/local/nginx/sbin/nginx -t
ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
 
[Install]
WantedBy=multi-user.target
EOF

systemctl enable nginx.service
systemctl start nginx.service


# -with-http_ssl_module

